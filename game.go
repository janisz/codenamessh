package main

import (
	"fmt"
	"github.com/charmbracelet/lipgloss"
	"math/rand"
	"sync"
)

const BOARDSIZE = 25

type tileType int

const (
	GREY tileType = iota
	RED
	BLUE
	FAIL
)

const (
	colorPrimary   = lipgloss.Color("#d7dadc")
	colorSecondary = lipgloss.Color("#626262")
	colorSeparator = lipgloss.Color("#9c9c9c")
	colorRed       = lipgloss.Color("#FF0000")
	colorBlue      = lipgloss.Color("#0000FF")
	colorGrey      = lipgloss.Color("#111111")
)

type move int

const (
	RedHint move = iota
	RedGuess
	BlueHint
	BlueGuess
	BlueFail
	RedFail
	Done
)

type userMove int

const (
	Hint userMove = iota
	Guess
	Wait
	Won
	Fail
	Err
)

var games []*game
var oldGames []*game
var lock sync.RWMutex

func newGame() *game {
	g := game{
		b:             newBoard(),
		hints:         []string{},
		turn:          false,
		redSpymaster:  "",
		blueSpymaster: "",
		blueTeam:      []string{},
		redTeam:       []string{},
	}
	return &g
}

func removeUser(name string) {
	lock.Lock()
	defer lock.Unlock()
	for _, g := range games {
		g.removeUser(name)
	}
}

func removeGame(gameToRemove *game) {
	lock.Lock()
	defer lock.Unlock()
	for i := range games {
		if games[i] == gameToRemove {
			games[i] = newGame()
		}
	}
}

func (g *game) removeUser(name string) {
	g.Lock()
	defer g.Unlock()
	if g.redSpymaster == name {
		g.redSpymaster = ""
		return
	}
	if g.blueSpymaster == name {
		g.blueSpymaster = ""
		return
	}
	newTeam := []string{}
	for _, n := range g.redTeam {
		if n == name {
			continue
		}
		newTeam = append(newTeam, n)
	}
	g.redTeam = newTeam

	newTeam = []string{}
	for _, n := range g.blueTeam {
		if n == name {
			continue
		}
		newTeam = append(newTeam, n)
	}
	g.blueTeam = newTeam
}

func (g *game) currentState() string {
	g.RLock()
	defer g.RUnlock()
	switch g.s {
	case RedHint:
		return lipgloss.NewStyle().Padding(0, 5).
			Border(lipgloss.NormalBorder()).Foreground(colorRed).Render("Red Spymaster gives a hint")
	case RedGuess:
		return lipgloss.NewStyle().Padding(0, 5).
			Border(lipgloss.NormalBorder()).Foreground(colorRed).Render("Red team guess words")
	case BlueHint:
		return lipgloss.NewStyle().Padding(0, 5).
			Border(lipgloss.NormalBorder()).Foreground(colorBlue).Render("Blue Spymaster gives a hint")
	case BlueGuess:
		return lipgloss.NewStyle().Padding(0, 5).
			Border(lipgloss.NormalBorder()).Foreground(colorBlue).Render("Blue team guess words")
	case Done, BlueFail, RedFail:
		return lipgloss.NewStyle().Padding(0, 5).
			Border(lipgloss.NormalBorder()).Foreground(colorPrimary).Render("Done")
	default:
		return lipgloss.NewStyle().Padding(0, 5).
			Border(lipgloss.NormalBorder()).Foreground(colorPrimary).Blink(true).Render("Unknown state")
	}
}

func (g *game) state(name string) userMove {
	g.RLock()
	defer g.RUnlock()
	var isRed, isSpymaster bool
	if g.redSpymaster == name {
		isRed = true
		isSpymaster = true
	} else if g.blueSpymaster == name {
		isRed = false
		isSpymaster = true
	} else {
		isSpymaster = false
		for _, n := range g.redTeam {
			if n == name {
				isRed = true
				break
			}
		}
	}

	if isRed && g.s == BlueFail {
		return Won
	}
	if isRed && g.s == RedFail {
		return Fail
	}
	if !isRed && g.s == BlueFail {
		return Fail
	}
	if !isRed && g.s == RedFail {
		return Won
	}

	if g.s == Done {
		red, blue := g.b.score()
		if red > blue && isRed {
			return Won
		}
		if red > blue && !isRed {
			return Fail
		}
		if blue > red && !isRed {
			return Won
		}
		if blue > red && isRed {
			return Fail
		}
	}

	switch g.s {
	case RedHint:
		if isRed && isSpymaster {
			return Hint
		}
		return Wait
	case RedGuess:
		if isRed && !isSpymaster {
			return Guess
		}
		return Wait
	case BlueHint:
		if !isRed && isSpymaster {
			return Hint
		}
		return Wait
	case BlueGuess:
		if !isRed && !isSpymaster {
			return Guess
		}
		return Wait
	default:
		return Err
	}
}

func (g *game) giveHint(h string) {
	g.Lock()
	defer g.Unlock()
	g.hints = append(g.hints, h)
	if g.s == RedHint {
		g.s = RedGuess
	} else {
		g.s = BlueGuess
	}
}

func (g *game) getWord(x, y int) string {
	g.RLock()
	defer g.RUnlock()
	return g.b[y*5+x].word
}

func (g *game) blinkWord(x, y, px, py int) string {
	g.Lock()
	defer g.Unlock()
	t := g.b[y*5+x]
	t.blink++
	g.b[py*5+px].blink--
	return t.word
}

func (g *game) unblinkWord(x, y int) string {
	g.Lock()
	defer g.Unlock()
	t := g.b[y*5+x]
	t.blink--
	return t.word
}

func (g *game) skip() {
	g.Lock()
	defer g.Unlock()
	if g.s == RedGuess {
		g.s = BlueHint
	} else {
		g.s = RedHint
	}
}

func (g *game) guess(x, y int) {
	g.Lock()
	defer g.Unlock()
	t := g.b.click(x, y)
	for _, t := range g.b {
		t.blink = 0
	}
	switch t.typ {
	case FAIL:
		if g.s == BlueGuess {
			g.s = BlueFail
		}
		g.s = RedFail
	case RED:
		if g.s == BlueGuess {
			g.s = RedHint
		}
	case BLUE:
		if g.s == RedGuess {
			g.s = BlueHint
		}
	case GREY:
		if g.s == RedGuess {
			g.s = BlueHint
		} else {
			g.s = RedHint
		}
	}
	red, blue := g.b.score()
	if red == 9 || blue == 8 {
		g.s = Done
	}
}

func (g *game) viewHints() string {
	g.RLock()
	defer g.RUnlock()
	hints := g.hints

	hintsList := []string{listHeader("HINTS")}
	for _, h := range hints {
		hintsList = append(hintsList, listDone(h))
	}

	lists := lipgloss.JoinHorizontal(lipgloss.Top,
		list.Copy().Render(
			lipgloss.JoinVertical(lipgloss.Left,
				hintsList...,
			),
		),
	)

	return lists
}

type game struct {
	sync.RWMutex
	s             move
	b             *board
	hints         []string
	turn          bool
	redSpymaster  string
	blueSpymaster string
	blueTeam      []string
	redTeam       []string
}

func (g *game) isSpymaster(name string) bool {
	g.Lock()
	defer g.Unlock()
	return g.redSpymaster == name || g.blueSpymaster == name
}

func (g *game) addPlayer(name string) {
	g.Lock()
	defer g.Unlock()

	names := append(append([]string{g.redSpymaster, g.blueSpymaster}, g.blueTeam...), g.redTeam...)
	for _, n := range names {
		if n == name {
			return
		}
	}

	if g.redSpymaster == "" {
		g.redSpymaster = name
		return
	}
	if g.blueSpymaster == "" {
		g.blueSpymaster = name
		return
	}
	if len(g.blueTeam) == len(g.redTeam) {
		g.redTeam = append(g.redTeam, name)
		return
	}
	g.blueTeam = append(g.blueTeam, name)
}

func (g *game) isEmpty() bool {
	g.RLock()
	defer g.RUnlock()
	return g.redSpymaster == ""
}

func (g *game) view(blink bool) string {
	g.RLock()
	defer g.RUnlock()
	if g.isEmpty() {
		return lipgloss.JoinHorizontal(lipgloss.Top,
			list.Copy().Blink(blink).Render(
				lipgloss.JoinVertical(lipgloss.Left,
					listHeaderBlue("EMPTY"),
				),
			),
			list.Copy().Blink(blink).Width(columnWidth).Render(
				lipgloss.JoinVertical(lipgloss.Left,
					listHeaderBlue("ROOM"),
				),
			),
		)
	}

	red, blue := g.b.score()

	blueRender := []string{listHeaderBlue(fmt.Sprintf("%s %d/%d", g.blueSpymaster, blue, 8))}
	for _, n := range g.blueTeam {
		blueRender = append(blueRender, listItem(n))
	}
	redRender := []string{listHeaderRed(fmt.Sprintf("%s %d/%d", g.redSpymaster, red, 9))}
	for _, n := range g.redTeam {
		redRender = append(redRender, listItem(n))
	}
	lists := lipgloss.JoinHorizontal(lipgloss.Top,
		list.Copy().Blink(blink).Render(
			lipgloss.JoinVertical(lipgloss.Left,
				redRender...,
			),
		),
		list.Copy().Blink(blink).Width(columnWidth).Render(
			lipgloss.JoinVertical(lipgloss.Left,
				blueRender...,
			),
		),
	)

	return lists
}

type board [25]*tile

type tile struct {
	typ    tileType
	hidden bool
	word   string
	blink  int
}

func newBoard() *board {
	wordsCount := len(wordlist)
	a := make([]int, wordsCount)
	for i := range a {
		a[i] = i
	}
	rand.Shuffle(wordsCount, func(i, j int) {
		a[i], a[j] = a[j], a[i]
	})
	b := board{}
	// RED
	for i := 0; i < 9; i++ {
		b[i] = &tile{
			typ:    RED,
			hidden: true,
			word:   wordlist[a[i]],
		}
	}

	for i := 9; i < 9+8; i++ {
		b[i] = &tile{
			typ:    BLUE,
			hidden: true,
			word:   wordlist[a[i]],
		}
	}

	for i := 9 + 8; i < 24; i++ {
		b[i] = &tile{
			typ:    GREY,
			hidden: true,
			word:   wordlist[a[i]],
		}
	}

	b[24] = &tile{
		typ:    FAIL,
		hidden: true,
		word:   wordlist[a[24]],
	}

	rand.Shuffle(BOARDSIZE, func(i, j int) {
		b[i], b[j] = b[j], b[i]
	})
	b[0].blink = 1

	return &b
}

func (b *board) click(x, y int) *tile {
	b[y*5+x].hidden = false
	return b[y*5+x]
}

func (b *board) isOver() bool {
	redCount := 0
	blueCount := 0
	fail := false
	for _, t := range b {
		if t.hidden {
			continue
		}
		switch t.typ {
		case RED:
			redCount++
		case BLUE:
			blueCount++
		case FAIL:
			fail = true
		}
	}
	const maxRedCount = 9
	const maxBlueCount = 8
	return fail || redCount == maxRedCount || blueCount == maxBlueCount
}

func (b *board) score() (red, blue int) {
	redCount := 0
	blueCount := 0
	for _, t := range b {
		if t.hidden {
			continue
		}
		switch t.typ {
		case RED:
			redCount++
		case BLUE:
			blueCount++
		}
	}
	return redCount, blueCount
}

func (g *game) viewBoard(spymaster bool) string {
	g.RLock()
	defer g.RUnlock()
	return g.b.view(spymaster)
}

func (b *board) view(spymaster bool) string {
	rows := lipgloss.JoinVertical(
		lipgloss.Center,
		b.viewRow(0, spymaster),
		b.viewRow(1, spymaster),
		b.viewRow(2, spymaster),
		b.viewRow(3, spymaster),
		b.viewRow(4, spymaster),
	)
	return lipgloss.NewStyle().
		Border(lipgloss.NormalBorder()).
		BorderForeground(colorSeparator).
		Padding(0, 1).
		Render(rows)
}

func (b *board) viewRow(i int, spymaster bool) string {
	words := b[5*i : 5*i+5]
	wordsRendered := []string{}
	for _, word := range words {
		wordsRendered = append(wordsRendered, word.view(spymaster))
	}
	return lipgloss.JoinHorizontal(lipgloss.Bottom, wordsRendered...)
}

func (t *tile) view(spymaster bool) string {
	c := t.color()
	if t.hidden && !spymaster {
		c = colorSecondary
	}
	word := t.word
	if t.blink > 0 {
		word = fmt.Sprintf("%s (%d)", word, t.blink)
	}
	return lipgloss.NewStyle().
		Padding(1, 7-len(word)/2, 1, 7-ceil(len(word))).
		Blink(t.blink > 0).
		Border(lipgloss.RoundedBorder()).
		BorderForeground(c).
		Foreground(c).
		Render(word)
}

func (t *tile) color() lipgloss.Color {
	switch t.typ {
	case GREY:
		return colorGrey
	case FAIL:
		return colorPrimary
	case RED:
		return colorRed
	case BLUE:
		return colorBlue
	default:
		return colorSecondary
	}
}

func ceil(a int) int {
	return (a + 1) / 2
}
