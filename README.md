
1. Generate keys for org

```abash
gh api https://api.github.com/orgs/stackrox/members | jq '.[].login' -r | xargs -IID bash -c "echo ID: && curl -sSL https://github.com/ID.keys" | sed 's/ssh-/\t - ssh-/g' > ssh.yml
```