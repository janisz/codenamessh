package main

// An example Bubble Tea server. This will put an ssh session into alt screen
// and continually print up to date terminal information.

import (
	"context"
	"fmt"
	"github.com/charmbracelet/lipgloss"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/wish"
	bm "github.com/charmbracelet/wish/bubbletea"
	lm "github.com/charmbracelet/wish/logging"
	"github.com/gliderlabs/ssh"
	"gopkg.in/yaml.v3"
)

const (
	host = "localhost"
	port = 23234
)

var keys = map[string][]ssh.PublicKey{}

func main() {
	games = []*game{newGame(), newGame()}

	rand.Seed(time.Now().Unix())
	println(lipgloss.Place(0, 0, lipgloss.Center, lipgloss.Center, newBoard().view(false)))
	println(lipgloss.Place(0, 0, lipgloss.Center, lipgloss.Center, newBoard().view(true)))

	k, err := readKeys("ssh.yml")
	if err != nil {
		log.Fatalln(err)
	}
	keys = k

	s, err := wish.NewServer(
		wish.WithAddress(fmt.Sprintf("%s:%d", host, port)),
		wish.WithHostKeyPath(".ssh/term_info_ed25519"),
		wish.WithPublicKeyAuth(func(ctx ssh.Context, key ssh.PublicKey) bool {
			return true
		}),
		wish.WithMiddleware(
			bm.Middleware(teaHandler),
			lm.Middleware(),
			RemoveUser(),
		),
	)
	if err != nil {
		log.Fatalln(err)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	log.Printf("Starting SSH server on %s:%d", host, port)
	go func() {
		if err = s.ListenAndServe(); err != nil {
			log.Fatalln(err)
		}
	}()

	<-done
	log.Println("Stopping SSH server")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer func() { cancel() }()
	if err := s.Shutdown(ctx); err != nil {
		log.Fatalln(err)
	}
}

func RemoveUser() wish.Middleware {
	return func(sh ssh.Handler) ssh.Handler {
		return func(s ssh.Session) {
			username := getUsername(s)
			sh(s)
			removeUser(username)
		}
	}
}

func readKeys(filename string) (map[string][]ssh.PublicKey, error) {
	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	k := map[string][]string{}
	err = yaml.Unmarshal(buf, k)
	if err != nil {
		return nil, fmt.Errorf("in file %q: %v", filename, err)
	}

	output := map[string][]ssh.PublicKey{}

	for login, publicKeys := range k {
		output[login] = []ssh.PublicKey{}
		for _, k := range publicKeys {
			parsedKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(k))
			if err != nil {
				return nil, err
			}
			output[login] = append(output[login], parsedKey)
		}
	}

	return output, nil
}

// You can wire any Bubble Tea model up to the middleware with a function that
// handles the incoming ssh.Session. Here we just grab the terminal info and
// pass it to the new model. You can also return tea.ProgramOptions (such as
// tea.WithAltScreen) on a session by session basis.
func teaHandler(s ssh.Session) (tea.Model, []tea.ProgramOption) {
	username := getUsername(s)
	pty, _, active := s.Pty()
	if !active {
		wish.Fatalln(s, "no active terminal, skipping")
		return nil, nil
	}
	m := model{
		username: username,
		term:     pty.Term,
		width:    pty.Window.Width,
		height:   pty.Window.Height,
		g:        games[0],
	}
	return m, []tea.ProgramOption{tea.WithAltScreen()}
}

func getUsername(s ssh.Session) string {
	username := s.User()
	for login, publicKeys := range keys {
		for _, k := range publicKeys {
			switch {
			case ssh.KeysEqual(s.PublicKey(), k):
				username = login
			}
		}
	}
	return username
}

type step int

const (
	hello step = iota
	choosegame
	confirm
	play
)

// Just a generic tea.Model to demo terminal information of ssh.
type model struct {
	input        string
	buffer       string
	username     string
	term         string
	width        int
	height       int
	g            *game
	s            step
	x, y         int
	prevX, prevY int
}

type refresh struct {
}

func tick() tea.Msg {
	time.Sleep(1 * time.Second)
	return refresh{}
}

func (m model) Init() tea.Cmd {
	return tick
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	if m.username == "" {
		n := rand.Intn(len(wordlist))
		name := wordlist[n]
		m.username = name
	}
	switch msg := msg.(type) {
	case refresh:
		m.prevX, m.prevY = m.x, m.y
		return m, tick
	case tea.WindowSizeMsg:
		m.height = msg.Height
		m.width = msg.Width
	case tea.KeyMsg:
		m.prevX, m.prevY = m.x, m.y
		gamesNumber := len(games)
		switch msg.String() {
		case tea.KeyCtrlC.String():
			return m, tea.Quit
		case tea.KeyBackspace.String():
			if m.buffer == "" {
				return m, nil
			}
			m.buffer = m.buffer[:len(m.buffer)-1]
		case tea.KeyUp.String():
			switch m.s {
			case choosegame:
				for i, g := range games {
					if g == m.g {
						m.g = games[(i+1)%gamesNumber]
						break
					}
				}
			case play:
				m.y = (m.y - 1 + 5) % 5
			}
		case tea.KeyRight.String():
			switch m.s {
			case choosegame:
				for i, g := range games {
					if g == m.g {
						m.g = games[(i+1)%gamesNumber]
						break
					}
				}
			case play:
				m.x = (m.x + 1) % 5
			}
		case tea.KeyDown.String():
			switch m.s {
			case choosegame:
				for i, g := range games {
					if g == m.g {
						m.g = games[(i+gamesNumber-1)%gamesNumber]
						break
					}
				}
			case play:
				m.y = (m.y + 1) % 5
			}
		case tea.KeyLeft.String():
			switch m.s {
			case choosegame:
				for i, g := range games {
					if g == m.g {
						m.g = games[(i+gamesNumber-1)%gamesNumber]
						break
					}
				}
			case play:
				m.x = (m.x - 1 + 5) % 5
			}
		case tea.KeyEscape.String():
			switch m.s {
			case play:
				switch m.g.state(m.username) {
				case Guess:
					m.g.skip()
					return m, nil
				default:
					return m, nil
				}
			}
		case tea.KeyEnter.String():
			switch m.s {
			case hello:
				if m.g.isEmpty() {
					m.s = confirm
				} else {
					m.s = choosegame
				}
			case choosegame:
				m.s = confirm
			case confirm:
				m.s = play
				m.g.addPlayer(m.username)
			case play:
				switch m.g.state(m.username) {
				case Hint:
					m.g.giveHint(m.buffer)
					m.buffer = ""
				case Guess:
					m.g.guess(m.x, m.y)
					return m, nil
				case Fail, Won:
					removeGame(m.g)
					return m, tea.Quit
				default:
					return m, nil
				}
			}
			m.input = m.buffer
			m.buffer = ""
		default:
			m.buffer += msg.String()
		}
	}
	return m, nil
}

func (m model) View() string {

	switch m.s {
	case hello:
		okButton := activeButtonStyle.Render("OK")

		question := lipgloss.NewStyle().Width(50).Align(lipgloss.Center).Render("Welcome " + m.username + " to Codenames!")
		buttons := lipgloss.JoinHorizontal(lipgloss.Top, okButton)
		ui := lipgloss.JoinVertical(lipgloss.Center, question, buttons)

		dialog := lipgloss.Place(m.width, m.height,
			lipgloss.Center, lipgloss.Center,
			dialogBoxStyle.Render(ui),
		)
		return dialog
	case choosegame:
		renderedGames := []string{}
		for _, g := range games {
			renderedGames = append(renderedGames, g.view(g == m.g))
		}
		ui := lipgloss.JoinVertical(lipgloss.Center, renderedGames...)
		list := lipgloss.Place(m.width, m.height,
			lipgloss.Center, lipgloss.Center,
			dialogBoxStyle.Render(ui),
		)
		return list
	case confirm:
		okButton := activeButtonStyle.Render("YES")

		question := lipgloss.NewStyle().Width(50).Align(lipgloss.Center).Render(m.username + " are you ready to play?")
		buttons := lipgloss.JoinHorizontal(lipgloss.Top, okButton)
		ui := lipgloss.JoinVertical(lipgloss.Center, question, buttons)

		dialog := lipgloss.Place(m.width, m.height,
			lipgloss.Center, lipgloss.Center,
			dialogBoxStyle.Render(ui),
		)
		return dialog
	case play:
		comment := ""
		switch m.g.state(m.username) {
		case Hint:
			comment = "Your hint: (word number)"
		case Guess:
			comment = fmt.Sprintf("Your guess: %s os ESC to pass", m.g.blinkWord(m.x, m.y, m.prevX, m.prevY))
		case Fail:
			okButton := activeButtonStyle.Render("Noooo")

			question := lipgloss.NewStyle().Width(50).Align(lipgloss.Center).Render(m.username + " lost match :(")
			buttons := lipgloss.JoinHorizontal(lipgloss.Top, okButton)
			ui := lipgloss.JoinVertical(lipgloss.Center, question, buttons)

			dialog := lipgloss.Place(m.width, m.height,
				lipgloss.Center, lipgloss.Center,
				dialogBoxStyle.Render(ui),
			)
			return dialog
		case Won:
			okButton := activeButtonStyle.Render("🏆")

			question := lipgloss.NewStyle().Width(50).Align(lipgloss.Center).Render(m.username + " you won the game!")
			buttons := lipgloss.JoinHorizontal(lipgloss.Top, okButton)
			ui := lipgloss.JoinVertical(lipgloss.Center, question, buttons)

			dialog := lipgloss.Place(m.width, m.height,
				lipgloss.Center, lipgloss.Center,
				dialogBoxStyle.Render(ui),
			)
			return dialog
		case Wait:
			comment = "Wait"
		default:
			comment = "Something went wrong :("
		}
		ui := lipgloss.JoinVertical(lipgloss.Top,
			lipgloss.Place(m.width/2, 3, lipgloss.Center, lipgloss.Center, m.g.currentState()),
			lipgloss.JoinHorizontal(lipgloss.Top, m.g.b.view(m.g.isSpymaster(m.username)),
				lipgloss.JoinVertical(lipgloss.Top, m.g.view(false), m.g.viewHints())),
			comment,
			m.buffer,
		)
		return lipgloss.Place(m.width, m.height,
			lipgloss.Center, lipgloss.Center,
			dialogBoxStyle.Render(ui),
		)
	default:
		return "err"

	}
}
